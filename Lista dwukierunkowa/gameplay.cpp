#include "stdafx.h"
#include "deck.h"
#include "gameplay.h"

void game::gwiazdka(deck kupka, deck kupka_prev)
{
	if (stan == 0)
		color(7);
	else if (stan == 1)
		color(2);
	else if (stan == 2)
		color(3);
	card *p;
	card *p2;
	int x, y;
	p2 = kupka_prev.HEAD;
	p = kupka.HEAD;
	x = kupka_prev.xh;
	y = kupka_prev.yh;
	y += 1;
	x += 3;
	gotoxy(x, y);
	cout << " ";
	x = kupka.xh;
	y = kupka.yh;
	y += 1;
	x += 3;
	gotoxy(x, y);
	cout << "*";
	color(7); // bia�y
}

void game::gwiazdka_down(deck kupka, deck kupka_prev)
{

	card *p;
	card *p2;
	int x, y;
	p2 = kupka_prev.HEAD;
	p = kupka.HEAD;
	x = kupka_prev.x;
	y = kupka_prev.y;
	y += 1;
	x += 3;
	gotoxy(x, y);
	cout << " ";
	x = kupka.x;
	y = kupka.y;
	y += 1;
	x += 3;
	gotoxy(x, y);
	cout << "*";
}

bool game::check_1_down(deck kupka)
{
	bool foo=false;
	card *p=kupka.HEAD;
	if (p) // czy pusta
	{
		if (p->kolor == 1 || p->kolor == 2) // kier/karo(czerwone)
		{
			if (p->next->kolor == 0 || p->next->kolor == 3)
			{
				if (p->next->value == p->value + 1)
					foo = true;
			}
			else
				foo = false;
		}

		if (p->kolor == 0 || p->kolor == 3) // pik/trefl(czarne)
		{
			if (p->next->kolor == 1 || p->next->kolor == 2)
			{
				if (p->next->value == p->value + 1)
					foo = true;
			}
			else
				foo = false;
		}
		for (int i = 1; i < il_kart; i++)
		{
			p = p->next;
			if (p->kolor == 1 || p->kolor == 2) // kier/karo(czerwone)
			{
				if (p->next->kolor == 0 || p->next->kolor == 3)
				{
					if (p->next->value == p->value + 1)
						foo = true;
				}
				else
					foo = false;
			}

			if (p->kolor == 0 || p->kolor == 3) // pik/trefl(czarne)
			{
				if (p->next->kolor == 1 || p->next->kolor == 2)
				{
					if (p->next->value == p->value + 1)
						foo = true;
				}
				else
					foo = false;
			}
		}
	}
		return foo;
}


bool game::check_1_up(deck kupka)
{
	bool foo = false;
	card *p = kupka.HEAD;
	if (p)// czy pusta
	{
		if (p->kolor == 1 || p->kolor == 2) // kier/karo(czerwone)
		{
			if (p->next->kolor == 0 || p->next->kolor == 3)
			{
				if (p->next->value == p->value + 1)
					foo = true;
			}
			else
				foo = false;
		}

		if (p->kolor == 0 || p->kolor == 3) // pik/trefl(czarne)
		{
			if (p->next->kolor == 1 || p->next->kolor == 2)
			{
				if (p->next->value == p->value + 1)
					foo = true;
			}
			else
				foo = false;
		}
		for (int i = 1; i < il_kart; i++)
		{
			p = p->next;
			if (p->kolor == 1 || p->kolor == 2) // kier/karo(czerwone)
			{
				if (p->next->kolor == 0 || p->next->kolor == 3)
				{
					if (p->next->value == p->value + 1)
						foo = true;
				}
				else
					foo = false;
			}

			if (p->kolor == 0 || p->kolor == 3) // pik/trefl(czarne)
			{
				if (p->next->kolor == 1 || p->next->kolor == 2)
				{
					if (p->next->value == p->value + 1)
						foo = true;
				}
				else
					foo = false;
			}
		}
	}
	return foo;
}



bool game::check_2_down(deck kupka,deck reka)
{
	bool foo = false;
	card *p = kupka.HEAD;
	card *r = reka.TAIL;
	//cout << "reka tail: "; reka.HEAD->display();
	//cout << "\nkupka head :"; kupka.TAIL->display();

		if (prev_marker != marker)
		{
			if (p)
			{
			if (p->kolor == 1 || p->kolor == 2) // kier/karo(czerwone)
			{
				if (r->kolor == 0 || r->kolor == 3)
				{
					if (r->value + 1 == p->value)
						foo = true;
				}
				else
					foo = false;
			}

			else if (p->kolor == 0 || p->kolor == 3) // pik/trefl(czarne)
			{
				if (r->kolor == 1 || r->kolor == 2)
				{
					if (r->value + 1 == p->value)
					{
						foo = true;
					}
				}
				else
				{
					foo = false;
				}
			}
			else
				foo = false;
			ruchy++;
	}
	else
		if (reka.TAIL->value == 13)
			foo = true;
	}
		else
			foo = true;
	return foo;
}


bool game::check_2_up(deck kupka, deck reka)
{
	bool foo = true;
	card *p = kupka.HEAD;
	card *r = reka.TAIL;
	//cout << "reka tail: "; reka.HEAD->display();
	//cout << "\nkupka head :"; kupka.TAIL->display();
	if (kupka.HEAD) //sprawdzanie czy pusta
	{
		if (prev_marker != marker)
		{
			if (p->kolor == 0 && r->kolor == 0) // pik
			{
				if (r->value == p->value + 1)
					foo = true;
				else
					foo = false;
			}

			else if (p->kolor == 1 && r->kolor == 1) // kier
			{
				if (r->value == p->value + 1)
					foo = true;
				else
					foo = false;
			}

			else if (p->kolor == 2 && r->kolor == 2) // karo
			{
				if (r->value == p->value +1 )
					foo = true;
				else
					foo = false;
			}

			else if (p->kolor == 3 && r->kolor == 3) // trefl
			{
				if (r->value == p->value + 1)
					foo = true;
				else
					foo = false;
			}
			else
				foo = false;
			ruchy++;
		}
	}
		else // jak pusta to tylko AS
		{
			if (reka.HEAD->value == 1 && reka.counter==1)
				foo = true;
			else
			foo = false;
			ruchy++;
	}
	return foo;
}

bool game::check_main(deck kupka)
{
	if (kupka.HEAD) // czy pusta
	{
		return true;
	}
	return false;
}

bool game::is_full(deck kupka)
{
	if (kupka.counter == 13)
		return true;
	else
		return false;

}

void game::ShowConsoleCursor(bool showFlag)
{
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_CURSOR_INFO     cursorInfo;

	GetConsoleCursorInfo(out, &cursorInfo);
	cursorInfo.bVisible = showFlag; // set the cursor visibility
	SetConsoleCursorInfo(out, &cursorInfo);
}