#pragma once
#include "stdafx.h"
#include "deck.h"
#include <iostream>
#include <cstdlib>
#include <windows.h>
extern unsigned int marker;
extern unsigned int prev_marker;
extern unsigned int stan;
extern unsigned int il_kart;
extern unsigned int ruchy;
extern HANDLE hOut;
class game
{
	//friend class deck;
public:
void ShowConsoleCursor(bool showFlag);
bool check_1_up(deck kupka);
bool check_1_down(deck kupka);
bool check_2_up(deck kupka, deck reka);
bool check_2_down(deck kupka, deck reka);
bool check_main(deck kupka);
bool is_full(deck kupka);
void gwiazdka_down(deck kupka, deck kupka_prev);
void gwiazdka(deck kupka, deck kupka_prev);
};
