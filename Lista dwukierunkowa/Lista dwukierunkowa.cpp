// decka dwukierunkowa.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "deck.h"
#include "player.h"
#include <ctime>
#include <conio.h>
#include "gameplay.h"
#include <algorithm>
//******************************************************************************

unsigned int marker = 0;
unsigned int prev_marker = 0;
unsigned int il_kart;
unsigned int stan = 0;
unsigned int ruchy = 0;
HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);

int main(int argc, _TCHAR* argv[])
{
	game game1;
	int n;
	char klaw = 0;
	clock_t tp;
	clock_t ts;
	double tc;
	double tcs;
	deck prev_kupka;
	deck *prev_d;
	unsigned int prev_m;
	deck talia1, kupki[13]; //od lewego dolu a od 7 gora
	deck reka;
	card *p;
	card *c;
	p = new card;
	player gracz;
	bool over = false;
	int points = 0;
	int * foo;
	talia1.genDeck();
	talia1.shuffle();
	game1.ShowConsoleCursor(false);
	SetConsoleTitle(TEXT("Pasjans"));
	//talia1.display();
	//SetConsoleTextAttribute(hOut, FOREGROUND_RED);
	//cout << "Ten napis jest z czerwonym tlem i zwyk�ym tekstem" << flush << endl << endl;

	//przenoszenie kart z potasowanej tali do kupek kart
	kupki[0].push_back(talia1.pop_front());
	kupki[0].setxy(0,10);
	
	for(n=0;n<2;n++)
	{
		kupki[1].push_back(talia1.pop_front());
		kupki[1].setxy(10,10);
	}
	
	for(n=0;n<3;n++)
	{
		kupki[2].push_back(talia1.pop_front());
		kupki[2].setxy(20,10);
		
	}
	
	for(n=0;n<4;n++)
	{
		kupki[3].push_back(talia1.pop_front());
		kupki[3].setxy(30,10);
		
	}
	
	for(n=0;n<5;n++)
	{
		kupki[4].push_back(talia1.pop_front());
		kupki[4].setxy(40,10);
	}
	
	for(n=0;n<6;n++)
	{
		kupki[5].push_back(talia1.pop_front());
		kupki[5].setxy(50,10);
		
	}
	for(n=0;n<7;n++)
	{
		kupki[6].push_back(talia1.pop_front());
		kupki[6].setxy(60,10);
		
	}
	
	for(n=0;n<24;n++)
	{	
		p = talia1.pop_front();
		p->visible = 1;
		kupki[7].push_back(p);
		kupki[7].setxy(0,0);
	}
	kupki[0].setxy(0, 10);
	kupki[1].setxy(10, 10);
	kupki[2].setxy(20, 10);
	kupki[3].setxy(30, 10);
	kupki[4].setxy(40, 10);
	kupki[5].setxy(50, 10);
	kupki[6].setxy(60, 10);
	kupki[7].setxy(0, 0);
	kupki[8].setxy(10, 0);
	kupki[9].setxy(30, 0);
	kupki[10].setxy(40, 0);
	kupki[11].setxy(50, 0);
	kupki[12].setxy(60, 0);
	reka.setxy(82, 25);


	kupki[0].displayDown(reka);
	kupki[1].displayDown(reka);
	kupki[2].displayDown(reka);
	kupki[3].displayDown(reka);
	kupki[4].displayDown(reka);
	kupki[5].displayDown(reka);
	kupki[6].displayDown(reka);
	kupki[6].displayDown(reka);
	kupki[7].displayUp();
	kupki[8].displayUp();
	kupki[8].displayUp();
	kupki[9].displayUp();
	kupki[10].displayUp();
	kupki[11].displayUp();
	kupki[12].displayUp();

	//OPIS
	gotoxy(75, 27);
	cout << "Reka: ";

	gotoxy(95, 27);
	cout << "Ruchy: ";
	cout << ruchy;

	gotoxy(95, 29);
	cout << "Czas:  ";
	cout << ruchy;

	gotoxy(85, 10);
	cout << "LEGENDA:";
	gotoxy(85, 13);
	cout << "PIK:    **";
	gotoxy(85, 14);
	cout << "KIER:   <3";
	gotoxy(85, 15);
	cout << "KARO:   <>";
	gotoxy(85, 16);
	cout << "TREFL:  -%";

	gotoxy(100, 13);
	cout << "* - wybieranie kupki";
	gotoxy(100, 14);
	color(2);
	cout << "* - wybieranie ilosci kart";
	gotoxy(100, 15);
	color(3);
	cout << "* - przenoszenie kart";
	//
	//PETLA GRY
	prev_kupka = kupki[0];
	game1.gwiazdka(kupki[0],prev_kupka);
	reka.displayUp();
	tp = clock();
	ts = clock();
	do // glowna petla
	{
		if (_kbhit())
		{
			klaw = _getch();


			//*****************************************************************************
			if (stan == 0) //stan 0
			{
				switch (klaw)
				{

				case 75: // lewa strzalka
				{
					if (marker != 0 && marker != 7)
					{
						marker--;
						game1.gwiazdka(kupki[marker],prev_kupka);
						prev_kupka = kupki[marker];
					}
					break;
				}

				case 77: // prawa strzalka
				{
					if (marker != 6 && marker != 12)
					{
						marker++;
						game1.gwiazdka(kupki[marker], prev_kupka);
						prev_kupka = kupki[marker];
					}
					//kupki[marker]->gwiazda();
					break;
				}

				case 72:  // gorna strzalka
				{
					if (marker < 7 && marker < 2)
						marker += 7;
					else if (marker < 7 && marker >= 2) // ustawienie poza kupkami
						marker += 6;
					game1.gwiazdka(kupki[marker], prev_kupka);
					prev_kupka = kupki[marker];
					break;
				}

				case 80: // dolna strzalka
				{

					if (marker >= 7 && marker < 9)
						marker -= 7;
					else if (marker >= 7 && marker >= 9) // ustawienie poza kupkami
						marker -= 6;
					game1.gwiazdka(kupki[marker], prev_kupka);
					prev_kupka = kupki[marker];
					break;
				}
				
				case 13: // enter
				{
					if (kupki[marker].HEAD)
					{
						if (marker != 7)
						{
							color(2);
							stan = 1;
							game1.gwiazdka(kupki[marker], prev_kupka);
							prev_kupka = kupki[marker];
							color(7);
							il_kart = 1;
							c = kupki[marker].HEAD;
							c->color = 2;
							prev_d = &kupki[marker];
							prev_m = marker;
							if (marker < 7)
								kupki[marker].displayDown(reka);
							else
								kupki[marker].displayUp();
						}
						else
						{
							if (game1.check_main(kupki[marker])) //marker = 7
							{
								kupki[8].push_front(kupki[7].pop_front());
								kupki[7].displayUp();
								kupki[8].displayUp();
								ruchy++;
							}
						}
					}
					else
					{
						if (marker == 7)
						{
							while (kupki[8].HEAD)
							{
								reka.push_back(kupki[8].pop_front());
							}
							while (reka.HEAD)
							{
								kupki[7].push_back(reka.pop_back()); //push_front??
							}
							reka.displayDown(reka);
							kupki[7].displayUp();
							kupki[8].displayUp();
						}
					}
					break;
				}

				case 27: //escape
				{
					color(7);
					stan = 0;
					game1.gwiazdka(kupki[marker], prev_kupka);
					prev_kupka = kupki[marker];
					il_kart = 0;
					break;
				}
				}
				prev_marker = marker;
			} // KONIEC STANU 0


			//*******************************************************************************
			else if (stan == 1)
			{

				switch (klaw)
				{

				case 72:  // gorna strzalka
				{
					//c = kupki[marker].HEAD;
					if (marker < 7)
					{
						if (il_kart < kupki[marker].counter)
						{
							if (game1.check_1_down(kupki[marker]))
							{
								c=c->next;
								c->color = 2;
									kupki[marker].displayDown(reka);
								il_kart += 1;
							}
						}
					}
					else if (marker == 8)
					{

					}
					break;
				}

				case 80: // dolna strzalka
				{
					if (il_kart > 1)
					{
						
						c->color = 0;
						kupki[marker].displayDown(reka);
						c = c->prev;
							il_kart -= 1;
					}
					break;
				}

				case 13: // enter
				{
					card *c; 
					//c = kupki[marker].HEAD; 
					if (marker < 7)
					{

						for (int i = 0; i < il_kart; i++)
						{
							c = kupki[marker].HEAD;
							c->color = 0;
							reka.push_back(kupki[marker].pop_front()); // front to dol tego co widac
							//prev_d.pop_front();
							//prev_d.pop_front();
						}
						kupki[marker].displayDown(reka);
						
					}
					else
					{
						for (int i = 0; i < il_kart; i++)
						{
							c = kupki[marker].HEAD;
							c->color = 0;
							reka.push_back(kupki[marker].pop_front()); // front to dol tego co widac
							//prev_d.pop_front();
						}
						kupki[marker].displayUp();
					}
					color(3);
					stan = 2;
					game1.gwiazdka(kupki[marker], prev_kupka);
					prev_kupka = kupki[marker];
					 reka.displayDown(reka);
					break;
				}

				case 27: //escape
				{
					stan = 0;
					//il_kart = 0;
					while (il_kart > 0)
					{
						c->color = 0;
						c = c->prev;
						il_kart -= 1;
					}
					if (marker<7)
						kupki[marker].displayDown(reka);
					else
						kupki[marker].displayUp();
					game1.gwiazdka(kupki[marker], prev_kupka);
					prev_kupka = kupki[marker];
					break;
				}
				}
			} // KONIEC STANU 1
			//************************************************************
			else if (stan == 2)
			{

				switch (klaw)
				{

				case 75: // lewa strzalka
				{
					if (marker != 0 && marker != 7)
					{
						marker--;
						game1.gwiazdka(kupki[marker], prev_kupka);
						prev_kupka = kupki[marker];
					}
					break;
				}

				case 77: // prawa strzalka
				{
					if (marker != 6 && marker != 12)
					{
						marker++;
						game1.gwiazdka(kupki[marker], prev_kupka);
						prev_kupka = kupki[marker];
					}
					//kupki[marker]->gwiazda();
					break;
				}

				case 72:  // gorna strzalka
				{
					if (marker < 7 && marker < 2)
						marker += 7;
					else if (marker < 7 && marker >= 2) // ustawienie poza kupkami
						marker += 6;
					game1.gwiazdka(kupki[marker], prev_kupka);
					prev_kupka = kupki[marker];
					break;
				}

				case 80: // dolna strzalka
				{

					if (marker >= 7 && marker < 9)
						marker -= 7;
					else if (marker >= 7 && marker >= 9) // ustawienie poza kupkami
						marker -= 6;
					game1.gwiazdka(kupki[marker], prev_kupka);
					prev_kupka = kupki[marker];
					break;
				}

				case 13: // enter
				{
					if (marker < 7)
					{
						if (game1.check_2_down(kupki[marker], reka))
						{
							for (int i = 0; i < il_kart; i++)
								kupki[marker].push_front(reka.pop_back());
							kupki[marker].displayDown(reka);
							il_kart = 0;
							stan = 0;
							if (prev_m != marker)
							{
								if (!reka.HEAD && prev_m!=8)
								{
									if (prev_d->HEAD)
									{
										prev_d->HEAD->visible = 1;
										if (prev_m < 7)
											prev_d->displayDown(reka);
										else
											prev_d->displayUp();
									}
								}
							}
							reka.displayDown(reka);
							game1.gwiazdka(kupki[marker], prev_kupka);
							prev_kupka = kupki[marker];
							//ruchy++;
						}
						
					}
					else if (marker>8)
					{
						if (game1.check_2_up(kupki[marker], reka))
						{
							for (int i = 0; i < il_kart; i++)
								kupki[marker].push_front(reka.pop_back());
							kupki[marker].displayUp();
							il_kart = 0;
							stan = 0;
							if (prev_m != marker)
							{
								if (!reka.HEAD && prev_m!=8)
								{
									if (prev_d->HEAD)
									{
										prev_d->HEAD->visible = 1;
										if (prev_m < 7)
											prev_d->displayDown(reka);
										if (prev_m > 7)
											prev_d->displayUp();
									}
								}
							}
						    reka.displayUp();
							game1.gwiazdka(kupki[marker], prev_kupka);
							prev_kupka = kupki[marker];
							//ruchy++;
						}
					}
					else if (marker == 8)
					{
						if (prev_marker == marker)
						{
							kupki[marker].push_front(reka.pop_back());
							kupki[marker].displayUp();
							il_kart = 0;
							stan = 0;
							reka.displayUp();
							game1.gwiazdka(kupki[marker], prev_kupka);
							prev_kupka = kupki[marker];
							//ruchy++;
						}
					}

					break;
				}

				case 27: //escape
				{
					/*for (int i = 0; i < il_kart; i++)
					{

					}
					stan = 0;
					il_kart = 0;*/
					break;
				}
				}
			} // KONIEC STANU 2
			//************************************************************
			
			/*cout << "STAN: " << stan << " ";
			cout << marker << "   IL_kart:" << il_kart;
			cout << endl;*/
			gotoxy(102, 27);
			cout << ruchy;
			} // Koniec obslugi klawiatury
			if (game1.is_full(kupki[9]) && game1.is_full(kupki[10]) && game1.is_full(kupki[11]) && game1.is_full(kupki[12]))
				over = true;
			tc = float((clock() - tp)) / CLOCKS_PER_SEC;
			tcs = float((clock() - ts)) / CLOCKS_PER_SEC;
			if (tcs>1)
			{
				ts = clock();
				gotoxy(102, 29);
				tc = int((clock() - tp)) / CLOCKS_PER_SEC;
				cout << tc;
			}
	} while (!over);
	tc = float((clock() - tp)) / CLOCKS_PER_SEC;
	system("cls");
	gotoxy(57, 25);
	cout << "GRATULACJE!!!";
	gotoxy(57, 27);
	cout << "ILOSC RUCHOW: " << ruchy;
	gotoxy(57, 29);
	cout << "CZAS: " << tc;
	_getch();


	//KONIEC PETLI GRY
	
	
	
	//TESTY//TESTY//TESTY//TESTY//TESTY//TESTY//TESTY//TESTY//TESTY//TESTY//TESTY//TESTY
	
	system("pause");
	return 0;
}

//******************************************************************************
